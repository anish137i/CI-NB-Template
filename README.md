  
CI-NB-Template Verison 1.0
==================

This is netbeans custom template created for codeigniter repetitive command can
can be type using following key bindings.

--------------------
### Release Download From :
https://goo.gl/skrziV
--------------------
### More Info On
http://www.anishmandal.in
--------------------

|Shortcut|Use For|
| --- |----|
|* civer|CI-NB Template Version Info|
|* php|Get php tag in html view|
|* cianchor|Codeigniter Anchor|
|* cicontroller|Codeigniter Controller|
|* cicookie|Codeigniter Fetch Cookie Data|
|* cidbaffected|Database Affected Rows|
|* cidbcount|Database Count All|
|* cidbdelete|Codeigniter AR delete|
|* cidbget|Codeigniter AR Get|
|* cidbgroup|Codeigniter AR group|
|* cidbinsert|Codeigniter AR insert|
|* cidbinsertbatch|Codeigniter AR insert batch|
|* cidbinsertid|Database Insert Id|
|* cidbjoin|Codeigniter AR Join|
|* cidblast|Database last Query|
|* cidblimit|Codeigniter AR limit **|
|* cidbload|Codeigniter Load Database|
|* cidbquery|Codeigniter Standard Query|
|* cidbselect|Codeigniter AR select|
|* cidbupdate|Codeigniter AR update|
|* cidbwhere|Codeigniter AR where **|
|* ciflash|Flash Data|
|* ciformcheckbox|Form checkbox|
|* ciformclose|Form Close|
|* ciformdropdown|Form dropdown|
|* ciformhidden|Form hidden|
|* ciforminput|Form input|
|* ciformlabel|Form label|
|* ciformopen|Form Open|
|* ciformpass|Form password|
|* ciformsubmit|Form submit|
|* ciformtext|Form text|
|* ciformupload|Form upload|
|* ciget|Codeigniter GET|
|* cigetpost|Codeigniter GET_POST|
|* cilibrary|Codeigniter Library|
|* ciloadhelper|Load helper|
|* ciloadlibrary|Load library|
|* ciloadmodel|Load Model|
|* ciloadview|Load View|
|* cimodel|Codeigniter Model|
|* cipost|Codeigniter POST|
|* ciserver|Codeigniter Fetch Server Data|
|* cisesall|Session All Userdata|
|* cisesdata|Session Userdata|
|* cisesdestroy|Session destroy|
|* cisesset|Session Set Userdata|
|* cisesunset|Session unset userdata|
|* cisetcookie|Set Cookie Data|
|* civalidation|Run set validation|
|* cirun|Run form validation|

-------------------

